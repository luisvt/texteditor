#include "texteditor.h"
#include "ui_texteditor.h"
#include <QMessageBox>
#include <QFileDialog>
#include <QFileInfo>
#include <QDebug>
#include <QSettings>
#include <QList>
#include <QtCore>

TextEditor::TextEditor(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::TextEditor)
{
    ui->setupUi(this);
    readSettings();

    for(int i=0;i<MaxRecenFiles;i++){
        recentFilesActs[i] = new QAction(this);
        recentFilesActs[i]->setVisible(false);
        connect(recentFilesActs[i],SIGNAL(triggered()),this,SLOT(openRecentFile()));
        ui->menuOpen_Recent_Files->addAction(recentFilesActs[i]);
    }

    updateRecentFileActions();
}


bool TextEditor::on_actionSave_as_triggered()
{
    QString fileName = QFileDialog::getSaveFileName(
                this,
                "TextEditor - Save as",
                "/home/luis/Documents",
                "Text Files (*.txt);;All Files (*.*)");
    if(!fileName.isEmpty()){
        curFile = fileName;
        return saveFile(fileName);
    }
    return false;
}

bool TextEditor::saveFile(const QString &fileName)
{
    QFile file(fileName);
    if(file.open(QFile::WriteOnly)){
        file.write(ui->plainTextEdit->toPlainText().toLatin1());
        setCurrentFile(fileName);
        setWindowTitle(tr("Text Editor - %1[*]").arg(QFileInfo(curFile).fileName()));
        ui->plainTextEdit->document()->setModified(false);
        return true;
    }else{
        QMessageBox::warning(
                    this,
                    "TextEditor",
                    tr("Cannot write file %1.\nError: %2")
                    .arg(curFile)
                    .arg(file.errorString()));
        return false;
    }
}

bool TextEditor::maybeSave()
{
    if(ui->plainTextEdit->document()->isModified()){
        QMessageBox::StandardButton ret =
                QMessageBox::warning(
                    this,
                    "TextEditor",
                    tr("The Document has been Modified"
                    "\nDo you want to save your changes?"),
                    QMessageBox::Yes | QMessageBox::No |QMessageBox::Cancel);
        if(ret == QMessageBox::Yes){
            return on_action_Save_triggered();
        }else if(ret == QMessageBox::Cancel)
            return false;
    }
    return true;
}

void TextEditor::readSettings()
{
    QSettings settings("Nicatronica","TextEditor");
    QPoint pos = settings.value("pos",QPoint(200,200)).toPoint();
    QSize size = settings.value("size",QSize(400,400)).toSize();
    resize(size);
    move(pos);
}

void TextEditor::writeSettings()
{
    QSettings settings("Nicatronica","TextEditor");
    settings.setValue("pos",pos());
    settings.setValue("size",size());
}

void TextEditor::setCurrentFile(const QString &fileName)
{
    curFile = fileName;

    setWindowTitle(tr("Text Editor - %1[*]").arg(QFileInfo(curFile).fileName()));

    QSettings settings;
    QStringList recentFilesList = settings.value("recentFilesList").toStringList();
    recentFilesList.removeAll(fileName);
    recentFilesList.prepend(fileName);
    while(recentFilesList.size() > MaxRecenFiles)
        recentFilesList.removeLast();

    settings.setValue("recentFilesList",recentFilesList);
    updateRecentFileActions();
}

bool TextEditor::on_action_Save_triggered()
{
    if(curFile.isEmpty())
        return on_actionSave_as_triggered();
    else
        return saveFile(curFile);
}

void TextEditor::on_action_Open_triggered()
{
    if(maybeSave()){
        QString fileName = QFileDialog::getOpenFileName(
                    this,
                    "TextEditor - Open file",
                    "/home/luis/Documents",
                    "Text Files (*.txt);;All Files (*.*)");
        loadFile(fileName);
    }
}

void TextEditor::on_action_New_triggered()
{
    if(maybeSave()){
        ui->plainTextEdit->clear();
    }
}

void TextEditor::closeEvent(QCloseEvent *event)
{
    if(maybeSave()){
        writeSettings();
        event->accept();
    }else{
        event->ignore();
    }
}

void TextEditor::openRecentFile()
{
    QAction *action = qobject_cast<QAction *>(sender());
    if(action){
        loadFile(action->data().toString());
    }
}


void TextEditor::updateRecentFileActions()
{
    QSettings settings;
    QStringList recentFilesList = settings.value("recentFilesList").toStringList();

    int numRecentFiles = qMin(recentFilesList.size(),int(MaxRecenFiles));

    for(int i = 0; i< numRecentFiles; ++i){
        QString text = tr("&%1 %2")
                .arg(i+1)
                .arg(QFileInfo(recentFilesList[i]).fileName());
        recentFilesActs[i]->setText(text);
        recentFilesActs[i]->setData(recentFilesList[i]);
        recentFilesActs[i]->setVisible(true);
    }
    for(int j= numRecentFiles; j>MaxRecenFiles;j++){
        recentFilesActs[j]->setVisible(false);
    }
}

void TextEditor::loadFile(const QString &fileName)
{
    if(!fileName.isEmpty()){
        QFile file(fileName);
        if(file.open(QFile::ReadOnly)){
            ui->plainTextEdit->setPlainText(file.readAll());
            setCurrentFile(fileName);
        }else{
            QMessageBox::warning(
                        this,
                        "TextEditor",
                        tr("Cannot read File %1.\nError: %2")
                        .arg(fileName)
                        .arg(file.errorString()));
        }
    }
}

void TextEditor::on_fordwardSearchBtn_clicked()
{
    ui->plainTextEdit->find(ui->searchLineEdit->text());
}

void TextEditor::on_backwardSearchBtn_clicked()
{
    ui->plainTextEdit->find(ui->searchLineEdit->text(),QTextDocument::FindBackward | QTextDocument::FindCaseSensitively | QTextDocument::FindWholeWords);
}
