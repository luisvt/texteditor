#-------------------------------------------------
#
# Project created by QtCreator 2013-03-30T12:41:07
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = TextEditor
TEMPLATE = app


SOURCES += main.cpp\
        texteditor.cpp

HEADERS  += texteditor.h

FORMS    += texteditor.ui

RESOURCES += \
    Resources.qrc
