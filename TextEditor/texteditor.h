#ifndef TEXTEDITOR_H
#define TEXTEDITOR_H

#include <QMainWindow>

namespace Ui {
class TextEditor;
}

class TextEditor : public QMainWindow
{
    Q_OBJECT
    
public:
    explicit TextEditor(QWidget *parent = 0);
    
private slots:

    bool on_actionSave_as_triggered();

    bool on_action_Save_triggered();

    void on_action_Open_triggered();

    void on_action_New_triggered();

    void closeEvent(QCloseEvent *event);

    void openRecentFile();


    void on_fordwardSearchBtn_clicked();

    void on_backwardSearchBtn_clicked();

private:
    Ui::TextEditor *ui;
    bool saveFile(const QString &fileName);
    bool maybeSave();
    void readSettings();
    void writeSettings();
    void setCurrentFile(const QString &fileName);
    void updateRecentFileActions();
    void loadFile(const QString &fileName);
    QString curFile;

    enum {MaxRecenFiles = 5};
    QAction *recentFilesActs[MaxRecenFiles];
};

#endif // TEXTEDITOR_H
